from django.contrib import admin
from voting_system.models import Participant, Vote, number

admin.site.register(Participant)
admin.site.register(Vote)
admin.site.register(number)
