# Generated by Django 2.1.4 on 2018-12-11 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voting_system', '0002_remove_vote_judge_num'),
    ]

    operations = [
        migrations.CreateModel(
            name='number',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num', models.PositiveSmallIntegerField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='vote',
            name='score',
        ),
        migrations.AddField(
            model_name='vote',
            name='s1',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s2',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s3',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s4',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s5',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s6',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s7',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vote',
            name='s8',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
