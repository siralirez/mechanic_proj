from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import login as dj_login, logout as dj_logout
from voting_system.forms import Login
from voting_system.models import Participant, Vote, number
from voting_system.forms import StandardsForm


class login(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return redirect('/admin')
            return redirect('/index')
        return render(request, 'login.html')

    def post(self, request):
        if request.user.is_authenticated:
            return redirect('/index')
        form = Login(request.POST)
        if form.is_valid():
            dj_login(request, form.user_cache)
            if request.GET.get('next', False):
                next = request.GET.get('next')
                return redirect(next)
            if request.user.is_superuser:
                return redirect('/admin')
            return redirect('/index')
        return render(request, 'login.html')


class logout(View):
    def get(self, request):
        dj_logout(request)
        return redirect('/login')

    def post(self, request):
        dj_logout(request)
        return redirect('/login')


class index(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        pass


class list(LoginRequiredMixin, View):
    def get(self, request):
        participant = Participant.objects.all().order_by('full_name')
        final_list = set()
        for p in participant:
            votes = Vote.objects.filter(participant=p.id, referee=request.user.id)
            for v in votes:
                if v.final:
                    final_list.add(p.full_name)
        return render(request, 'list.html', {'participant': participant, 'final_list': final_list})

    def post(self, request):
        pass


class vote(LoginRequiredMixin, View):
    def get(self, request):
        pid = request.GET.get('id')
        num = number.objects.get(pk=1).num  # raye nazer
        if pid is None or not Participant.objects.filter(pk=pid).exists():
            messages.add_message(request, messages.ERROR, 'شرکت کننده وجود ندارد.')
            return redirect('/list')
        else:
            name = Participant.objects.get(pk=pid).full_name
        if Vote.objects.filter(referee=request.user.id, participant=pid).exists() and not Vote.objects.get(
                referee=request.user.id, participant=pid).final:
            return render(request, 'vote.html', {'id': pid, 'flag1': False, 'flag2': True, 'num': num, 'name': name})
        elif Vote.objects.filter(referee=request.user.id, participant=pid).exists() and Vote.objects.get(
                referee=request.user.id, participant=pid).final:
            return render(request, 'vote.html', {'id': pid, 'flag1': False, 'flag2': False, 'num': num, 'name': name})
        return render(request, 'vote.html', {'id': pid, 'flag1': True, 'flag2': True, 'num': num, 'name': name})

    def post(self, request):
        pid = request.POST.get('id')
        form = StandardsForm(request.POST)
        if form.is_valid():
            if Vote.objects.filter(referee=request.user, participant_id=pid).exists():
                if not Vote.objects.get(referee=request.user, participant_id=pid).final:
                    Vote.objects.filter(referee=request.user, participant_id=pid).update(s1=form.cleaned_data['s1'],
                                                                                         s2=form.cleaned_data['s2'],
                                                                                         s3=form.cleaned_data['s3'],
                                                                                         s4=form.cleaned_data['s4'],
                                                                                         s5=form.cleaned_data['s5'],
                                                                                         s6=form.cleaned_data['s6'],
                                                                                         s7=form.cleaned_data['s7'],
                                                                                         s8=form.cleaned_data['s8'])
                else:
                    messages.add_message(request, messages.SUCCESS, 'امتیاز نهایی شده است و امکان تغییر وجود ندارد.')
                    return redirect('/vote?id={}'.format(pid))
            else:
                Vote.objects.create(referee=request.user, participant_id=pid,
                                    s1=form.cleaned_data['s1'], s2=form.cleaned_data['s2'], s3=form.cleaned_data['s3'],
                                    s4=form.cleaned_data['s4'], s5=form.cleaned_data['s5'], s6=form.cleaned_data['s6'],
                                    s7=form.cleaned_data['s7'], s8=form.cleaned_data['s8'])
            messages.add_message(request, messages.SUCCESS, 'امتیاز با موفقیت ثبت شد.')
            return redirect('/vote?id={}'.format(pid))
        messages.add_message(request, messages.ERROR, 'ثبت امتیاز با مشکل مواجه شد لطفا دوباره تلاش نمایید.')
        return redirect('/vote?id={}'.format(pid))


class scoreboard(View):
    def get(self, request):
        participant = Participant.objects.all()
        items = {}
        judges = {}
        for p in participant:
            num = Vote.objects.filter(participant_id=p.id).count()
            if num == 0:
                items[p.full_name] = 0
                continue
            else:
                votes = Vote.objects.filter(participant_id=p.id)
                num = number.objects.get(pk=1)  # raye nazer
                sum = 0
                judge_details = {}
                judge_sum = 0
                for v in votes:
                    judge_sum = sum
                    sum += v.s1 * 3
                    sum += v.s2 * 3
                    sum += v.s3 * 3
                    sum += v.s4 * 3
                    sum += v.s5 * 2
                    sum += v.s6 * 2
                    sum += v.s7 * 1
                    sum += v.s8 * 2
                    sum += num.num * 1
                    judge_sum = sum - judge_sum - num.num
                    judge_details[v.referee.id] = judge_sum
                # items[p.full_name] = round(sum / num)
                x = 100000
                while len(judge_details) < 6:
                    if len(judge_details) + 1 == 6:
                        judge_details[x] = num.num * len(votes)
                        break
                    judge_details[x] = 0
                    x += 1
                judges[p.full_name] = sorted(judge_details.items(), key=lambda kv: kv[0])
                items[p.full_name] = sum
        items = sorted(items.items(), key=lambda kv: kv[1])
        items.reverse()
        # print(judges)
        return render(request, 'scoreboard.html', {'participant': items, 'judges': judges})

    def post(self, request):
        pass


class main(View):
    def get(self, request):
        return render(request, 'main.html')

    def post(self, request):
        pass


class edit_vote(LoginRequiredMixin, View):
    def get(self, request):
        pid = request.GET.get('id')
        num = number.objects.get(pk=1).num  # raye nazer
        if pid is None or not Participant.objects.filter(pk=pid).exists():
            messages.add_message(request, messages.ERROR, 'شرکت کننده وجود ندارد.')
            return redirect('/list')
        else:
            name = Participant.objects.get(pk=pid).full_name
        if Vote.objects.filter(referee=request.user.id, participant=pid).exists() and Vote.objects.get(
                referee=request.user.id, participant=pid).final:
            return render(request, 'vote.html', {'id': pid, 'flag1': False, 'flag2': False, 'num': num, 'name': name})
        vote = Vote.objects.get(referee=request.user.id, participant=pid)

        return render(request, 'vote.html',
                      {'id': pid, 'flag1': True, 'flag2': True, 'flag3': True, 's1': vote.s1, 's2': vote.s2,
                       's3': vote.s3,
                       's4': vote.s4,
                       's5': vote.s5, 's6': vote.s6,
                       's7': vote.s7, 's8': vote.s8, 'num': num, 'name': name, 'rng': range(11)})


class final_vote(LoginRequiredMixin, View):
    def get(self, request):
        pid = request.GET.get('id')
        num = number.objects.get(pk=1).num  # raye nazer
        if pid is None or not Participant.objects.filter(pk=pid).exists():
            messages.add_message(request, messages.ERROR, 'شرکت کننده وجود ندارد.')
            return redirect('/list')
        else:
            name = Participant.objects.get(pk=pid).full_name
        if Vote.objects.filter(referee=request.user.id, participant=pid).exists():
            Vote.objects.filter(referee=request.user, participant_id=pid).update(final=1)
            messages.add_message(request, messages.SUCCESS, 'امتیاز با موفقیت نهایی شد.')
            return redirect('/vote?id={}'.format(pid))
        messages.add_message(request, messages.ERROR, 'رای ثبت نشده است.')
        return redirect('/vote?id={}'.format(pid))


class view_vote(LoginRequiredMixin, View):
    def get(self, request):
        pid = request.GET.get('id')
        num = number.objects.get(pk=1).num  # raye nazer
        if pid is None or not Participant.objects.filter(pk=pid).exists():
            messages.add_message(request, messages.ERROR, 'شرکت کننده وجود ندارد.')
            return redirect('/list')
        else:
            name = Participant.objects.get(pk=pid).full_name
        if Vote.objects.filter(referee=request.user.id, participant=pid).exists():
            vote = Vote.objects.get(referee=request.user.id, participant=pid)
            num = number.objects.get(pk=1)
            return render(request, 'view_vote.html',
                          {'name': name, 'id': pid, 'flag1': True, 'flag2': True, 's1': vote.s1, 's2': vote.s2,
                           's3': vote.s3,
                           's4': vote.s4,
                           's5': vote.s5, 's6': vote.s6,
                           's7': vote.s7, 's8': vote.s8, 'num': num.num})
        messages.add_message(request, messages.ERROR, 'رای ثبت نشده است.')
        return redirect('/vote?id={}'.format(pid))
